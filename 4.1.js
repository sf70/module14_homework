//Заметил особенность, про которую в курсе нет ни слова. Если в секции then раскомментить return (это в коде ниже), то последующий catch выпонится не зависимо от reject/resolve. В случае resolve выполнится и then и catch В связи с этим длинные цепочки из catch и then могут работать не корректно, но браузер на это не ругается. В общем как то корявенько работает этот механизм, если есть какие-то соглашения по размещению очередности секций then, catch это стоило упомянуть
function usePromise() {
  const myPromise = new Promise((resolve, reject) => {
    console.log('Промис начался');
    
    setTimeout(() => {
      console.log('2');
      var nonce = Math.floor(Math.random() * 100) + 1
      console.log('nonce - ', nonce);
      if (nonce % 2) {
        return reject({
          data: nonce
        })
      } else {
        return resolve({
          data: nonce
        })
      }
    }, 1000);
    console.log('Промис закончился');
  });


  myPromise
    .then((result) => {
      console.log('Завершено успешно. Сгенерированное число — ', result.data);
      return {data: result.data}
    })
    .catch((error) => {
      console.log('Завершено с ошибкой. Сгенерированное число — ', error.data);
    });
    // .then((result) => {
    //   console.log('Завершено успешно. Сгенерированное число!!!!! — ', result.data);
    //   // return {data: result.data}
    // });

}
console.log('Запускаем функцию с promise');
usePromise();
console.log('Функция выполнилась');











