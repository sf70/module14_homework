const xmlStr = `<list>
  <student>
    <name lang="en">
      <first>Ivan</first>
      <second>Ivanov</second>
    </name>
    <age>35</age>
    <prof>teacher</prof>
  </student>
  <student>
    <name lang="ru">
      <first>Петр</first>
      <second>Петров</second>
    </name>
    <age>58</age>
    <prof>driver</prof>
  </student>
</list>`;

const jsonStr = `{
  "list": [
   {
    "name": "Petr",
    "age": "20",
    "prof": "mechanic"
   },
   {
    "name": "Vova",
    "age": "60",
    "prof": "pilot"
   }
  ]
 }`;
class List {
  list = []
}

class FromXML extends List {
  constructor (xmlString = '') {
    super()
    const xmlDOM = new DOMParser().parseFromString(xmlString, "text/xml");
    const listNode = xmlDOM.querySelector("list");
    const studentsNode = listNode.querySelectorAll("student");
    for (let element of studentsNode) {
      let nameNode = element.querySelector("name");
      let lang = nameNode.getAttribute("lang");
      let firstName = nameNode.querySelector("first").textContent;
      let secondName = nameNode.querySelector("second").textContent;
      let ageNode = element.querySelector("age").textContent;
      let profNode = element.querySelector("prof").textContent;
      this.list.push({name: firstName+" "+secondName, age: ageNode, prof: profNode, lang: lang});
    }
  }
}
class FromJSON extends List {
  constructor (jsonString = '') {
    super()
    const data = JSON.parse(jsonString);
    const listData = data.list;
    for (let element of data.list) {
      this.list.push({name: element.name, age: element.age, prof: element.prof});
    }
  }
}


let xmlList = new FromXML(xmlStr)
console.log(xmlList)
let jsonList = new FromJSON(jsonStr)
console.log(jsonList)

