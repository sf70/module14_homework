//Кнопрка для срабатывания 
const form = document.querySelector('.panel');
const pictures = document.querySelector('.pictures');
// const btn = document.querySelector('.panel__submit');
const regexp = /\b([1-9]|10)\b/;
// var ids = localStorage.getItem("pictures").split(',');
picDrawer(localStorage.getItem("pictures").split(','))

form.addEventListener('submit', async function (event) {
  const data = new FormData(this)
  //Отключаю стандартное поведение формы
  event.preventDefault()
  //Валидация полей input
  const page = data.get('page')
  const testPage = page.length === 0 || regexp.test(page);
  const limit = data.get('limit')
  const testLimit = limit.length === 0 || regexp.test(limit);
  switch (true){
    case (!testPage && !testLimit):
      var d1 = document.getElementById('limit');
      d1.insertAdjacentHTML('afterend', '<label class="label__all valid__text">Номер страницы и лимит вне диапазона от 1 до 10</label>');
      setTimeout(() => {
        var labelAll = document.querySelector('.label__all');
        labelAll.remove();
      }, 3000);
      return false
    case !testPage:
      var d1 = document.getElementById('page');
      d1.insertAdjacentHTML('afterend', '<label class="label__page valid__text">Номер страницы вне диапазона от 1 до 10</label>');
      setTimeout(() => {
        var labelAll = document.querySelector('.label__page');
        labelAll.remove();
      }, 3000);
      return false
    case !testLimit:
      var d1 = document.getElementById('limit');
      d1.insertAdjacentHTML('afterend', '<label class="label__limit valid__text">Лимит вне диапазона от 1 до 10</label>');
      setTimeout(() => {
        var labelAll = document.querySelector('.label__limit');
        labelAll.remove();
      }, 3000);
      return false
  }
  //Если всё ОК, выполняется запрос
  var resp = await useRequest(page, limit)
  if (resp) {
    bla = localStorage.getItem("pictures")
    if (bla) {
      var ids = bla.split(',');
      ids.forEach(item => {
        localStorage.removeItem(item)
      })
    }
    var pictures = []
    resp.forEach(item => {
        pictures.push(item.id)
        localStorage.setItem(item.id, item.download_url)
    });
    localStorage.setItem("pictures", pictures)
    picDrawer(pictures)
  } else {
    return
  }

});

function useRequest(page, limit) {
  return fetch('https://picsum.photos/v2/list?page='+page+'&limit='+limit)
    .then((response) => {
      if(response.ok){
        return response.json();
      }
    })
    .catch((error) => { 
      var d1 = document.getElementById('submit');
      d1.insertAdjacentHTML('afterend', '<label class="label__submit valid__text">Не удалось выполнить запрос</label>');
      setTimeout(() => {
        var labelAll = document.querySelector('.label__submit');
        labelAll.remove();
      }, 3000);
    });
};

function picDrawer(pictur) {
  let pics = '';
  pictur.forEach(item => {
    const pic = `
      <div class="draw">
        <img
        src="${localStorage.getItem(item)}"
        class="draw__picture"
        />
      </div>
    `;
    pics += pic
  });
  
  pictures.innerHTML = pics;
}




