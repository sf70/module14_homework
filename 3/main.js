//Кнопрка для срабатывания 
const btn = document.querySelector('.panel__submit');
const pictures = document.querySelector('.pictures');

btn.addEventListener('click', () => {
  const input = document.querySelector('.panel__input').value;
  console.log(input);
  console.log(+input);
  switch (true) {
    case (+input >=1 && +input <= 10):
      useRequest(+input, picDrawer);
      break;
    case (+input <=0):
      alert("Пожалуйста введите целое положительное число!")
      break
    default:
      alert("Вы не попали в числовой диапазон!")
      break;
  }
});
function useRequest(limit, callback) {
  console.log("useRequest")
  var request = new XMLHttpRequest();
  request.open('GET', 'https://picsum.photos/v2/list?limit='+limit);
  request.onerror = function () {
    console.log("Что-то пошло не так!");
  };
  request.onload = function() {
    if (request.status != 200) {
      console.log('Статус ответа: ', request.status);
    } else {
      const result = JSON.parse(request.response);
      if (callback) {
        callback(result);
      }
    }
  };
  request.send();
  console.log("useRequest")
}

function picDrawer(picInfo) {
  let pics = '';
  // console.log('start cards', cards);
  
  picInfo.forEach(item => {
    const pic = `
      <div class="draw">
        <img
          src="${item.download_url}"
          class="draw__picture"
        />
        <p>${item.author}</p>
      </div>
    `;
    pics += pic;
  });
  pictures.innerHTML = pics;
}





